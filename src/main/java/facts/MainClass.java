package facts;

import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.meta.ApiContext;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Date;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

//http://muzey-factov.ru/
public class MainClass {
    private static String BOT_NAME_test = "@Nik_Gun_bot";
    private static String BOT_NAME = "@fact_room_bot";
    private static String BOT_TOKEN_test = "1228532829:AAFPmSuvoRfXPTwOSft6_PMGcPTUw10pee4";
    private static String BOT_TOKEN = "1334119482:AAEPMsCJvubS9dkgcidOBlpMix2I2DmNRYY";

    private static String PROXY_HOST = "124.226.214.105";
    private static Integer PROXY_PORT = 38801;

    public static void main(String[] args) {
        if (isTest()) {
            BOT_NAME = BOT_NAME_test;
            BOT_TOKEN = BOT_TOKEN_test;
        }
        System.out.println(getClassBuildTime());
        try {
            // getProxy();
            DataBase.getInstanse();
            ApiContextInitializer.init();

            // Create the TelegramBotsApi object to register your bots
            TelegramBotsApi botsApi = new TelegramBotsApi();

            // Set up Http proxy
            DefaultBotOptions botOptions = ApiContext.getInstance(DefaultBotOptions.class);

            botOptions.setProxyHost(PROXY_HOST);
            botOptions.setProxyPort(PROXY_PORT);
            // Select proxy type: [HTTP|SOCKS4|SOCKS5] (default: NO_PROXY)
            botOptions.setProxyType(DefaultBotOptions.ProxyType.NO_PROXY);

            // Register your newly created AbilityBot
            MyAbilityTelegramBot bot = new MyAbilityTelegramBot(BOT_TOKEN, BOT_NAME, botOptions);

            botsApi.registerBot(bot);

        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    /**
     * Handles files, jar entries, and deployed jar entries in a zip file (EAR).
     *
     * @return The date if it can be determined, or null if not.
     */
    private static Date getClassBuildTime() {
        Date d = null;
        Class<?> currentClass = new Object() {
        }.getClass().getEnclosingClass();
        URL resource = currentClass.getResource(currentClass.getSimpleName() + ".class");
        if (resource != null) {
            if (resource.getProtocol().equals("file")) {
                try {
                    d = new Date(new File(resource.toURI()).lastModified());
                } catch (URISyntaxException ignored) {
                }
            } else if (resource.getProtocol().equals("jar")) {
                String path = resource.getPath();
                d = new Date(new File(path.substring(5, path.indexOf("!"))).lastModified());
            } else if (resource.getProtocol().equals("zip")) {
                String path = resource.getPath();
                File jarFileOnDisk = new File(path.substring(0, path.indexOf("!")));
                //long jfodLastModifiedLong = jarFileOnDisk.lastModified ();
                //Date jfodLasModifiedDate = new Date(jfodLastModifiedLong);
                try (JarFile jf = new JarFile(jarFileOnDisk)) {
                    ZipEntry ze = jf.getEntry(path.substring(path.indexOf("!") + 2));//Skip the ! and the /
                    long zeTimeLong = ze.getTime();
                    Date zeTimeDate = new Date(zeTimeLong);
                    d = zeTimeDate;
                } catch (IOException | RuntimeException ignored) {
                }
            }
        }
        return d;
    }

    private static boolean isTest() {
        String command = "ifconfig -a";
        Process p = null;
        try {
            p = Runtime.getRuntime().exec(command);


            BufferedReader inn = new BufferedReader(new InputStreamReader(p.getInputStream()));

            while (true) {
                String line = inn.readLine();
                //System.out.println(line);
                if (line.contains("\tether b8:f6:b1:1a:4a:5d"))
                    return true;
                if (line == null)
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
