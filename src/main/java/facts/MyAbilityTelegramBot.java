package facts;

import org.apache.commons.lang3.time.DateUtils;
import org.telegram.abilitybots.api.bot.AbilityBot;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class MyAbilityTelegramBot extends AbilityBot {
    DataBase db;
    FactHelper factHelper;

    protected MyAbilityTelegramBot(String botToken, String botUsername, DefaultBotOptions botOptions) {
        super(botToken, botUsername, botOptions);
        db = DataBase.getInstanse();
        factHelper = new FactHelper();
        new NotifyTimer(this);

    }

    void sendTextMessage(Long to, String text) {
        try {
            SendMessage snd = new SendMessage();
            snd.setChatId(to);
            snd.setText(text);
            execute(snd);
        } catch (TelegramApiException e) {

        }
    }

    public void sendTextMessageWithInlineKeyBoard(Long to, String text, InlineKeyboardMarkup inlineKeyboardMarkup) {
        try {
            SendMessage snd = new SendMessage();
            snd.setChatId(to);
            snd.setText(text);
            snd.setReplyMarkup(inlineKeyboardMarkup);
            execute(snd);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }


    synchronized void sendMarkdownV2Message(Long to, String text) {
        try {
            SendMessage snd = new SendMessage();
            snd.setChatId(to);
            snd.setText(text);
            snd.setParseMode("markdown");
            execute(snd);

        } catch (TelegramApiException e) {

        }
    }

    void sendMessagePhoto(Long to, int photoId) {
        try {
            SendPhoto sendPhoto = new SendPhoto();
            sendPhoto.setPhoto("http://muzey-factov.ru/img/facts/" + photoId + ".png");
            sendPhoto.setChatId(to);
            execute(sendPhoto);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }

    void sendMessageWithDocument(Long to, int template) {
        try {
            // new ExcelCreateTable(to, template);
            SendDocument sendDocument = new SendDocument();
            File f = new File("excel/tables/" + to + ".xlsx");
            sendDocument.setDocument(f);

            sendDocument.setCaption("Вот список того что вы делали");
            sendDocument.setChatId(to);
            execute(sendDocument);
            f.delete();
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    void sendTextMessageWithKeyBoard(Long to, String text, ReplyKeyboardMarkup keyboardMarkup) {
        try {
            SendMessage snd = new SendMessage();
            snd.setChatId(to);
            snd.setText(text);
            snd.setReplyMarkup(keyboardMarkup);
            execute(snd);
        } catch (TelegramApiException e) {

        }
    }

    @Override
    public int creatorId() {
        return 448596921;
    }

    /**
     * Menu Description
     * -1 - меню
     * 1 - настройки
     * 2 - пауза
     */
    @Override
    public void onUpdatesReceived(List<Update> updates) {
        if (updates.size() >= 1) {

            Update update = updates.get(updates.size() - 1);
            Message message = update.getMessage();

            TgKeyBoardBuilder keyBoardBuilder = new TgKeyBoardBuilder();
            if (message != null) {
                Chat chat = message.getChat();
                String text = message.getText();
                System.out.println(getCurrentTime() + ":\t" + chat.getUserName() + ":\t" + text);
                Long id = chat.getId();
                db.addLog(id, text, getCurrentTime());
                if (!db.isUserExist(id)) {
                    db.addUser(id, chat.getUserName());
                    sendTextMessage((long) creatorId(), "Новый пользователь: " + chat.getUserName());
                    keyBoardBuilder.addTextButton("Еще факт", 1);
                    keyBoardBuilder.addTextButton("Не получать факты каждый день", 2);
                    if (id == creatorId()) {
                        keyBoardBuilder.addTextButton("Статистика", 3);
                        keyBoardBuilder.addTextButton("Юзеры", 3);
                    }
                    sendTextMessageWithKeyBoard(id, "Добро пожаловать в бот фактов!\nЯ буду присылать тебе по факту в день, а также ты можешь сам узнать факт,нажав на соответствующую кнопку\n\n Я беру факты с сайта http://muzey-factov.ru/", keyBoardBuilder.getKeyboardMarkup());
                } else {
                    if (text.equals("Выход")) {
                        db.removeUser(id);
                        keyBoardBuilder.addTextButton("Начать", 1);
                        sendTextMessageWithKeyBoard(id, "Вы вышли!", keyBoardBuilder.getKeyboardMarkup());
                    } else if (text.equalsIgnoreCase("Еще факт") || text.equalsIgnoreCase("факт")) {
                        sendNextFact(id);
                    } else if (text.equalsIgnoreCase("Не получать факты каждый день")) {

                        keyBoardBuilder.addTextButton("Еще факт", 1);
                        keyBoardBuilder.addTextButton("Снова получать факты каждый день", 2);
                        if (id == creatorId()) {
                            keyBoardBuilder.addTextButton("Статистика", 3);
                            keyBoardBuilder.addTextButton("Юзеры", 3);
                        }
                        sendTextMessageWithKeyBoard(id, "Ты всегда можешь получить новый факт нажав на кнопку!", keyBoardBuilder.getKeyboardMarkup());
                        db.setPause(id, true);
                    } else if (text.equalsIgnoreCase("Снова получать факты каждый день")) {
                        db.setPause(id, false);
                        keyBoardBuilder.addTextButton("Еще факт", 1);
                        keyBoardBuilder.addTextButton("Не получать факты каждый день", 2);
                        if (id == creatorId()) {
                            keyBoardBuilder.addTextButton("Статистика", 3);
                            keyBoardBuilder.addTextButton("Юзеры", 3);
                        }
                        sendTextMessageWithKeyBoard(id, "С возвращением!", keyBoardBuilder.getKeyboardMarkup());
                    } else if (id == creatorId() && text.equalsIgnoreCase("Статистика")) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Кол-во пользователей: ").append(db.getUsersID().size()).append("\n");
                        sb.append("Кол-во фактов: ").append(db.getFactsCount()).append("\n");
                        sb.append("Кол-во активыных пользователей: ").append(db.getActiveUsers(getCurrentTime().split(" ")[0])).append("\n");
                        sendTextMessage(id, sb.toString());
                    } else if (id == creatorId() && text.equalsIgnoreCase("Юзеры")) {
                        StringBuilder sb = new StringBuilder();
                        ArrayList<String> users = db.getUsersNick();
                        for (int i = 0; i < users.size(); i++) {
                            sb.append("@").append(users.get(i));
                            if (i == 5) {
                                sb.append("\n");
                            } else {
                                sb.append(", ");
                            }
                        }
                        sendTextMessage(id, sb.toString());
                    }
                }
            } else {
                Integer id = update.getCallbackQuery().getFrom().getId();
                String callBackText = update.getCallbackQuery().getData();
                if (callBackText.contains("done:")) {
                    callBackText = callBackText.split(":")[1];
                }
            }
        }
    }

    public boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            int d = Integer.parseInt(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    private int checkIsNotify(String text) {
        int n = -1;
        text = text.replace(" ", "");
        if (isNumeric(text.substring(1, 2))) {
            if (isNumeric(text.substring(1, 3))) {
                return Integer.parseInt(text.substring(1, 3));
            } else {
                return Integer.parseInt(text.substring(1, 2));
            }
        }

        return n;
    }

    private int checkIsCase(String text) {
        int n = 0;
        text = text.replace(" ", "");
        if (!text.contains("!")) {
            return n;
        } else {
            /*for (int i = 1; i < 5; i++) {
                if (text.substring(i - 1, i).equals("-")) {
                    n++;
                } else break;
            }*/
            for (int i = 0; i < 5; i++) {
                if (text.charAt(i) == '!') {
                    n++;
                } else break;
            }
        }
        return n;
    }


    String getCurrentTime() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        return sdf.format(cal.getTime());
    }


    String getModifyTime(String time, int d) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        try {
            Date t = sdf.parse(time.split(" ")[0]);
            time = sdf.format(DateUtils.addDays(t, d));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time;
    }

    String getModifyUnixTime(String time, int d) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        try {
            Date t = sdf.parse(time);
            time = sdf.format(DateUtils.addHours(t, d));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time;
    }

    @Override
    public void onClosing() {

    }

    public void sendNextFact(Long id) {
        Fact fact = factHelper.getFact(db.getUserFactsCount(id));
        sendMessagePhoto(id, fact.id);
        sendMarkdownV2Message(id, "*" + fact.qustion + "*\n\n" + fact.value + "\n[Источник](http://muzey-factov.ru/" + fact.id + ")");
        db.addUserFactHistory(id, fact.id);

    }
}
