package facts;

import org.apache.commons.lang3.time.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class NotifyTimer extends Thread {

    MyAbilityTelegramBot myAbilityTelegramBot;
    DataBase db;

    public NotifyTimer(MyAbilityTelegramBot bot) {
        myAbilityTelegramBot = bot;
        db = DataBase.getInstanse();
        this.start();
    }

    String lastH = "-1";


    public void run() {
        int index = 0;//[0] - часы [1] - минуты
        lastH = getCurrentTime().split(":")[index];
        while (true) {
            if (!lastH.equals(getCurrentTime().split(":")[index])) {
                lastH = getCurrentTime().split(":")[index];
                if (lastH.equals("10") ) {
                    ArrayList<Long> users = db.getUsersID();
                    for (Long user : users) {
                        if (!db.checkPause(user))
                            myAbilityTelegramBot.sendNextFact(user);
                    }
                }

            }
        }
    }

    String getCurrentTime() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        return sdf.format(cal.getTime());
    }

    String getModifyTime(String time, int m) {

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Date dtime = null;
        try {
            dtime = sdf.parse(time);
            dtime = DateUtils.addMinutes(dtime, m);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sdf.format(dtime.getTime());
    }
}
