package facts;


import java.sql.*;
import java.util.ArrayList;

public class DataBase {
    // JDBC URL, username and password of MySQL server
    private final String url = "jdbc:mysql://localhost:3306/";
    private final String user = "root";
    private final String password = "dBM6rW_c";
    //private final String password = "12345678";

    // JDBC variables for opening and managing connection
    private Connection con;
    private Statement stmt;
    private static DataBase db;
    private ResultSet resSet;

    void ConnectionToDB() {
        try {
            //  con = DriverManager.getConnection(url, user, password);
            //stmt = con.createStatement();
            con = null;
            try {
                Class.forName("org.sqlite.JDBC");
                con = DriverManager.getConnection("jdbc:sqlite:fact_bot.db");
                stmt = con.createStatement();
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            }


            //stmt.execute("CREATE DATABASE IF NOT EXISTS StaffDataBase;");
            //tmt.execute("USE StaffDataBase;");
            stmt.execute("CREATE TABLE IF NOT EXISTS Users(user_id BIGINT primary key NOT NULL,userNick TEXT,user_rank INTEGER,send_time TIME DEFAULT '10:00',pause INTEGER DEFAULT 0,language INTEGER DEFAULT 0,menu_state INT NOT NULL DEFAULT -1,isActive BOOLEAN);");
            stmt.execute("CREATE TABLE IF NOT EXISTS Log(user_id BIGINT,value TEXT,event_time TEXT,isActive BOOLEAN);");
            stmt.execute("CREATE TABLE IF NOT EXISTS History(user_id BIGINT,fact_id INTEGER,isActive BOOLEAN);");
            stmt.execute("CREATE TABLE IF NOT EXISTS Facts(id INTEGER,fact_id INTEGER primary key NOT NULL,value TEXT);");
            System.out.println("Database connected success");
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Database connection error");
        }
    }

    static DataBase getInstanse() {
        if (db == null) {
            db = new DataBase();
        }
        return db;
    }

    DataBase() {
        ConnectionToDB();
    }

    boolean isUserExist(Long user_id) {
        String queryGetId = "SELECT * FROM Users WHERE user_id=" + user_id + " ;";
        try {
            resSet = stmt.executeQuery(queryGetId);
            while (resSet.next()) {
                if (resSet.getInt("user_id") == user_id) {
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    boolean addUser(Long user_id, String nick) {
        String query = "INSERT INTO Users (user_id,userNick) VALUES (" + user_id + ",'" + nick + "'); ";
        try {
            stmt.execute(query);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    boolean addLog(Long user_id, String text, String time) {
        String query = "INSERT INTO Log (user_id,value,event_time) VALUES (" + user_id + ",'" + text + "','" + time + "'); ";
        try {
            stmt.execute(query);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


    void closeConnection() {
        try {
            con.close();
        } catch (Exception se) { /*can't do anything */ }
        try {
            if (stmt != null)
                stmt.close();
        } catch (SQLException se) { /*can't do anything */ }

    }

    public void setMenuState(Long from_id, int menu) {
        try {
            stmt.execute("UPDATE Users SET menu_state=" + menu + " WHERE user_id=" + from_id + ";");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    int getMenu(Long from_id) {
        try {
            resSet = stmt.executeQuery("SELECT * FROM Users WHERE user_id=" + from_id + ";");
            while (resSet.next()) {
                return resSet.getInt("menu_state");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void setRank(Long from_id, int rank) {
        try {
            stmt.execute("UPDATE Users SET user_rank=" + rank + " WHERE user_id=" + from_id + ";");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeUser(Long from_id) {
        try {
            stmt.execute("DELETE FROM Users WHERE user_id=" + from_id + ";");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void addEvent(Long user_id, String currentTime, String i) {
        String query = "INSERT INTO History (user_id,event_date,event_time,value) VALUES (" + user_id + ",'" + currentTime.split(" ")[0] + "','" + currentTime.split(" ")[1] + "','" + i + "'); ";
        try {
            stmt.execute(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public ArrayList<Long> getUsersID() {
        ArrayList<Long> result = new ArrayList<Long>();
        try {
            resSet = stmt.executeQuery("SELECT * FROM Users;");

            while (resSet.next()) {
                result.add(resSet.getLong("user_id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;

    }

    public int getLang(Long id) {
        try {
            resSet = stmt.executeQuery("SELECT * FROM Users WHERE user_id=" + id + ";");
            while (resSet.next()) {
                return resSet.getInt("language");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void setLang(long user_id, int lang) {
        try {
            stmt.execute("UPDATE Users SET language=" + lang + " WHERE user_id=" + user_id + ";");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addNewFact(int id) {
        String query = "INSERT INTO Facts (id,fact_id) VALUES ((SELECT IFNULL(MAX(id), 0) + 1 FROM Facts)," + id + "); ";
        try {
            stmt.execute(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public int getFactsCount() {
        int result = 0;
        try {
            resSet = stmt.executeQuery("SELECT COUNT(*) FROM Facts;");

            while (resSet.next()) {
                result = resSet.getInt(1);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return result;
    }

    public int getUserFactsCount(long id) {
        int result = 0;
        try {
            resSet = stmt.executeQuery("SELECT COUNT(*) FROM History WHERE user_id=" + id + ";");
            while (resSet.next()) {
                result = resSet.getInt(1);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return result;
    }

    public void addUserFactHistory(Long id, int id1) {
        String query = "INSERT INTO History (user_id,fact_id) VALUES (" + id + "," + id1 + "); ";
        try {
            stmt.execute(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getFactIdByRow(int row) {
        int result = 0;
        try {
            resSet = stmt.executeQuery("SELECT * FROM Facts WHERE id=" + (row + 1) + ";");
            while (resSet.next()) {
                result = resSet.getInt("fact_id");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return result;
    }

    public boolean checkPause(Long user_id) {
        int result = 0;
        try {
            resSet = stmt.executeQuery("SELECT * FROM Users WHERE user_id=" + user_id + ";");
            while (resSet.next()) {
                result = resSet.getInt("pause");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        if (result == 0) {
            return false;
        } else {
            return true;
        }

    }

    public void setPause(Long user_id, boolean b) {
        try {
            int myInt = b ? 1 : 0;
            stmt.execute("UPDATE Users SET pause=" + myInt + " WHERE user_id=" + user_id + ";");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getActiveUsers(String date) {
        int result = 0;
        try {
            resSet = stmt.executeQuery("SELECT * FROM Log WHERE event_time LIKE '" + date + "%' AND (value='факт' OR value='Еще факт' OR value='Факт');");
            String blackList = "";
            while (resSet.next()) {
                if (!blackList.contains(resSet.getLong("user_id") + "")) {
                    result++;//= resSet.getInt("pause");
                    blackList += resSet.getLong("user_id") + "/";
                }

            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return result;
    }

    public ArrayList<String> getUsersNick() {
        ArrayList<String> result = new ArrayList<>();
        try {
            resSet = stmt.executeQuery("SELECT * FROM Users;");

            while (resSet.next()) {
                result.add(resSet.getString("userNick"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
