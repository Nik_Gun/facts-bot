package facts;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class FactHelper {
    DataBase db;

    public FactHelper() {
        db = DataBase.getInstanse();
    }

    public Fact getFact(int row) {
        if (row+2>db.getFactsCount()){
            loadNewFacts();
        }
        int id = db.getFactIdByRow(row);
        if (id == 0) {
            loadNewFacts();
        }
        Fact fact = new Fact(id);
        String url = "http://muzey-factov.ru/" + id;
        Document doc;
        int tCount = 0;
        String tName;
        try {
            doc = Jsoup.connect(url).get();
            Elements newsHeadlines = doc.select("h2");
            //
            for (Element headline : newsHeadlines) {
                System.out.println(headline.childNode(0));
                fact.qustion = headline.childNode(0).toString();
            }
            newsHeadlines = doc.select(".content");
            for (Element headline : newsHeadlines) {
                fact.value = headline.childNode(0).toString();
            }
            /*newsHeadlines = doc.select(".content");
            for (Element headline : newsHeadlines) {
                fact.value = headline.childNode(0).toString();
            }*/
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fact;
    }

    public void loadNewFacts() {
        //String url = "http://muzey-factov.ru/";
        try {
            // Create a URL for the desired page
            //int page=;
            URL url = new URL("http://muzey-factov.ru/from" + db.getFactsCount());
            ArrayList<Integer> ids = new ArrayList<>();
            // Read all the text returned by the server
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            String str;
            while ((str = in.readLine()) != null) {
                str = in.readLine().toString();
                if (str.contains("<a name=")) {
                    String id = str.replace("<a name=\'", "").replace("\'></a>", "");
                    ids.add(Integer.parseInt(id));
                }

                // str is one line of text; readLine() strips the newline character(s)
            }
            in.close();
            for (int id : ids) {
                db.addNewFact(id);
            }
        } catch (MalformedURLException e) {
        } catch (IOException e) {
        }
    }

    public Fact getOldFact() {
        Fact result = null;

        return result;
    }


}
